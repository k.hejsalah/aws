// Importer les modules nécessaires
const http = require('http');
const AWS = require('aws-sdk');

// Configurer l'accès AWS
AWS.config.update({
  accessKeyId: 'VOTRE_ACCESS_KEY_ID',
  secretAccessKey: 'VOTRE_SECRET_ACCESS_KEY',
  region: 'VOTRE_REGION_AWS'
});

// Créer un serveur HTTP
const server = http.createServer((req, res) => {
  // Définir le code de statut et les en-têtes HTTP
  res.writeHead(200, {'Content-Type': 'text/html'});

  // Envoyer le contenu de la page
  res.end('<h1>Hello World!</h1>');
});

// Définir le port sur lequel le serveur écoutera
const port = 3000;

// Démarrer le serveur et écouter les connexions sur le port spécifié
server.listen(port, () => {
  console.log(`Serveur en cours d'exécution sur le port ${port}`);
});

// Exemple : Utiliser AWS S3
const s3 = new AWS.S3();

const params = {
  Bucket: 'NOM_DE_VOTRE_BUCKET',
  Key: 'NOM_DE_VOTRE_FICHIER'
};

s3.getObject(params, (err, data) => {
  if (err) {
    console.error(err);
  } else {
    console.log(data); // Afficher les données de l'objet récupéré depuis S3
  }
});
